import { Component,OnInit } from '@angular/core';

import { NavController } from 'ionic-angular';
import {ProfilePersonal} from "../profile.personal/profile.personal"

@Component({
  selector: 'page-generate-quote',
  templateUrl: 'generated-quote.html'
})
export class GenerateQuoteList implements OnInit{
 onboard:string;
  constructor(public navCtrl: NavController) {
    
  }
  ngOnInit(){
    this.onboard='signup';
  }

   signupNext(){
    this.navCtrl.push(ProfilePersonal);
  }


}
