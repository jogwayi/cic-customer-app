import { Component } from '@angular/core';
import { Http } from '@angular/http';
import { NavParams, NavController } from 'ionic-angular';

@Component({
  selector: 'page-policies-view',
  templateUrl: 'policies.view.html'
})
export class PolicyView {
  policy:Array<any>;

  constructor(public navCtrl: NavController,private http:Http, public navParams: NavParams) {
    this.policy = navParams.data['policy'];
  }
  ngOnInit(){
     //console.log(this.policy);
  }
  itemTapped(event, item) {
    
  }
  next(){
    
  }

}
