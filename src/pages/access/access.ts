import { Component,OnInit } from '@angular/core';
import { Dashboard } from '../dashboard/dashboard';
import { NavController,LoadingController } from 'ionic-angular';
import {ProfilePersonal} from "../profile.personal/profile.personal"

@Component({
  selector: 'page-access',
  templateUrl: 'access.html'
})
export class Access implements OnInit{
 onboard:string;
  constructor(public navCtrl: NavController,public loadingCtrl: LoadingController) {
    
  }
  ngOnInit(){
    this.onboard='signup';
  }

   signIn(){
     let loader = this.loadingCtrl.create({
      content: "Signing you in ...",
      //duration: 3000
    });
    loader.present();
    setTimeout(() => {
        loader.dismiss();
        this.navCtrl.setRoot(Dashboard,{}, {animate: true, direction: 'forward'});
      }, 1000);
  }
   signupNext(){
     let loader = this.loadingCtrl.create({
      content: "Signing you up ...",
      //duration: 3000
    });
    loader.present();
    setTimeout(() => {
        loader.dismiss();
        this.navCtrl.setRoot(ProfilePersonal,{}, {animate: true, direction: 'forward'});
      }, 1000);
  }


}
