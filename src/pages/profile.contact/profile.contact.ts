import { Component,OnInit } from '@angular/core';

import { NavController } from 'ionic-angular';

@Component({
  selector: 'profile.contact',
  templateUrl: 'profile.contact.html'
})
export class ProfileContact implements OnInit{
 onboard:string;
  constructor(public navCtrl: NavController) {
    
  }
  ngOnInit(){
    this.onboard='signup';
  }

}
