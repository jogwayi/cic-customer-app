import { Component,OnInit } from '@angular/core';

import { Products } from '../products/products';
import { Policies } from '../policies/policies';
import { GenerateQuote } from '../generate-quote/generate-quote';
import { NavController, NavParams } from 'ionic-angular';
declare var cordova: any;
@Component({
  selector: 'page-dashboard',
  templateUrl: 'dashboard.html',
  styles:[
    "#page { height: 100%;overflow: auto;}"
  ]
})
export class Dashboard implements OnInit{
  selectedItem: any;
  icons: string[];
  keyboard:string;
  items: Array<{title: string, note?: string, icon?: string,component:Component}>;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.items=[
        {
            title:'',
            note:'',
            icon:'',
            component:GenerateQuote
        },
        {
            title:'',
            note:'',
            icon:'',
            component:Policies
        },
    ]
  }
  ngOnInit(){
    
  }
  itemTapped(event, item) {
    console.log(item);
    // That's right, we're pushing to ourselves!
    //this.navCtrl.push(Page2, {
      //item: item
    //});
    this.navCtrl.setRoot(item.component,{}, {animate: true, direction: 'forward'});
  }
}
