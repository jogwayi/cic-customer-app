import { Component,OnInit } from '@angular/core';
import {Validators, FormGroup, FormBuilder } from '@angular/forms';
import { NavController,LoadingController } from 'ionic-angular';
import {ProfileContact} from "../profile.contact/profile.contact"
import {Access} from "../access/access"

@Component({
  selector: 'profile.personal',
  templateUrl: 'profile.personal.html'
})
export class ProfilePersonal implements OnInit{
 onboard:string;
 form:FormGroup;
  constructor(public navCtrl: NavController,public loadingCtrl: LoadingController,private formBuilder: FormBuilder) {
    this.form = this.formBuilder.group({
      documentType: ["", Validators.compose([Validators.required])],
      nationality: ["", Validators.compose([Validators.required])],
      maritalStatus:  ["", Validators.compose([Validators.required])],
      gender: ["", Validators.compose([Validators.required])],
      solution: ["", Validators.compose([Validators.required])],
      surname: ["", Validators.compose([Validators.required])],
      othername: ["", Validators.compose([Validators.required])],
      payrollNo: ["", Validators.compose([Validators.required])],
      documentNumber: ["", Validators.compose([Validators.required])],
      employer: ["", Validators.compose([Validators.required])],
      occupation: ["", Validators.compose([Validators.required])],
    });
  }
  ionViewLoaded() {
    
  }
  ngOnInit(){
    this.onboard='signup';
  }
  goBack(){
    this.navCtrl.setRoot(Access);
  }
  next(){
    console.log(this.form.value)
    console.log(this.form.valid)
    let loader = this.loadingCtrl.create({
      content: "Updating your personal details ...",
      //duration: 3000
    });
    loader.present();
    setTimeout(() => {
        loader.dismiss();
        this.navCtrl.push(ProfileContact);
      }, 1500);
  }

}
