import { Component } from '@angular/core';
import { Http } from '@angular/http';
import { NavParams, NavController } from 'ionic-angular';

@Component({
  selector: 'page-product-brief',
  templateUrl: 'product.brief.html',
})
export class GenerateQuoteProductBrief {
  policy:Array<any>;
  callback:any;
  constructor(public navCtrl: NavController,private http:Http, public navParams: NavParams) {
    this.policy = navParams.data['policy'];
  }
  ngOnInit(){
     //console.log(this.policy);
  }
  itemTapped(event, item) {
    
  }
  next(){
    
  }
 ionViewWillEnter() {
      this.callback = this.navParams.get("callback")
 }
  ionViewWillLeave() {
    this.policy['currentPage']=0
    this.callback(this.policy).then(()=>{
       //this.navCtrl.pop(); 
   }); 
  }

}
