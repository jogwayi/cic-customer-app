import { Component,OnInit } from '@angular/core';
import { Http } from '@angular/http';
import { NavController } from 'ionic-angular';
import {GenerateQuoteProductBrief} from "./product.brief/product.brief"

@Component({
  selector: 'page-generate-quote',
  templateUrl: 'generate-quote.html'
})
export class GenerateQuote implements OnInit{
  
   products:Array<any>;
   quoteContent:Object;
   callbackFunction = (_params) => {
    return new Promise((resolve, reject) => {
       //console.log(_params);
        this.quoteContent = _params;
        resolve();
    });
  }
  constructor(private http:Http,public navCtrl: NavController) {
    
  }
  ngOnInit(){
     this.http.get("assets/products.json").subscribe(products=>{
      console.log(products.json());
      this.products = products.json().AgentProducts;
    }) 
  }

   next(policy){
    this.navCtrl.push(GenerateQuoteProductBrief, {
      policy: policy,
      quoteContent:this.quoteContent,
      callback:this.callbackFunction
    });
  }
}
