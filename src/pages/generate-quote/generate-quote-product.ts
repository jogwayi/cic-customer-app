import { Component,OnInit } from '@angular/core';

import { NavController } from 'ionic-angular';
import {ProfilePersonal} from "../profile.personal/profile.personal"

@Component({
  selector: 'page-generate-quote-product',
  templateUrl: 'generate-quote-product.html'
})
export class GenerateQuoteProduct implements OnInit{
 onboard:string;
  constructor(public navCtrl: NavController) {
    
  }
  ngOnInit(){
    this.onboard='signup';
  }

   signupNext(){
    this.navCtrl.push(ProfilePersonal);
  }


}
