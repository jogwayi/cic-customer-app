import { Component,OnInit } from '@angular/core';
import { Http } from '@angular/http';
import { NavController, NavParams } from 'ionic-angular';
import {PolicyView} from "../policies.view/policies.view"
declare var cordova: any;
@Component({
  selector: 'page-policies',
  templateUrl: 'policies.html',
  styles:[
    "#page { height: 100%;overflow: auto;}"
  ]
})
export class Policies implements OnInit{
   selectedItem: any;
  icons: string[];
  keyboard:string;
  products:Array<any>;
  items: Array<{title: string, note: string, icon: string}>;

  constructor(public navCtrl: NavController,private http:Http, public navParams: NavParams) {
    
  }
  ngOnInit(){
    this.http.get("assets/products.json").subscribe(products=>{
      console.log(products.json());
      this.products = products.json().AgentProducts;
    }) 
  }
  viewPolicy(policy){
    this.navCtrl.push(PolicyView, {
      policy: policy
    });
  }
  itemTapped(event, item) {
    // That's right, we're pushing to ourselves!
    //this.navCtrl.push(Page2, {
      //item: item
    //});
  }
}
