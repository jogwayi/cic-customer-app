import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-faq',
  templateUrl: 'faq.html'
})
export class FaqPage {

  data: Array<{title: string, details: string, icon: string, showDetails: boolean}> = [];

  constructor(public navCtrl: NavController) {
    for(let i = 0; i < 20; i++ ){
      this.data.push({
          title: 'Title '+i,
          details: 'This is the FAQ description with a very long content. Can be replaced via a service.\r\nThis is the FAQ description with a very long content. Can be replaced via a service.\r\nThis is the FAQ description with a very long content. Can be replaced via a service.\r\nThis is the FAQ description with a very long content. Can be replaced via a service.\r\nThis is the FAQ description with a very long content. Can be replaced via a service.\r\n',
          icon: 'ios-add-circle-outline',
          showDetails: false
        });
    }
  }

  toggleDetails(data) {
    if (data.showDetails) {
        data.showDetails = false;
        data.icon = 'ios-add-circle-outline';
    } else {
        data.showDetails = true;
        data.icon = 'ios-remove-circle-outline';
    }
  }

}
