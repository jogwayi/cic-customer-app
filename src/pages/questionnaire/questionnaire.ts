import { Component,OnInit } from '@angular/core';

import { NavController } from 'ionic-angular';
import {ProfilePersonal} from "../profile.personal/profile.personal"

@Component({
  selector: 'page-questionnaire',
  templateUrl: 'questionnaire.html'
})
export class Questionnaire implements OnInit{
 onboard:string;
  constructor(public navCtrl: NavController) {
    
  }
  ngOnInit(){
    this.onboard='signup';
  }

   signupNext(){
    this.navCtrl.push(ProfilePersonal);
  }


}
