import { Component,OnInit } from '@angular/core';

import { NavController } from 'ionic-angular';

@Component({
  selector: 'policy.overview',
  templateUrl: 'policy.overview.html'
})
export class PolicyOverview implements OnInit{
 onboard:string;
  constructor(public navCtrl: NavController) {
    
  }
  ngOnInit(){
    this.onboard='signup';
  }

}
