import { NgModule } from '@angular/core';
import { IonicApp, IonicModule } from 'ionic-angular';
import { MyApp } from './app.component';
import { Dashboard } from '../pages/dashboard/dashboard';
import { Products } from '../pages/products/products';
import { Access } from '../pages/access/access';
import { GenerateQuote } from '../pages/generate-quote/generate-quote';
import { PolicyView } from '../pages/policies.view/policies.view';
import { Policies } from '../pages/policies/policies';
import {ProfileContact} from "../pages/profile.contact/profile.contact"
import {ProfilePersonal} from "../pages/profile.personal/profile.personal"
import {GenerateQuoteProductBrief} from "../pages/generate-quote/product.brief/product.brief"
import { FaqPage } from '../pages/faq/faq';

@NgModule({
  declarations: [
    MyApp,
    PolicyView,Access,ProfileContact,ProfilePersonal,FaqPage,
    Policies,Dashboard,Products,GenerateQuote,GenerateQuoteProductBrief
  ],
  imports: [
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,Products,GenerateQuote,GenerateQuoteProductBrief,
    PolicyView,Access,ProfileContact,ProfilePersonal,FaqPage,
    Policies,Dashboard
  ],
  providers: []
})
export class AppModule {}
