import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar, Splashscreen } from 'ionic-native';
import { Policies } from '../pages/policies/policies';
import { Access } from '../pages/access/access';
import { FaqPage } from '../pages/faq/faq';
import { Dashboard } from '../pages/dashboard/dashboard';


@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = Access;
  backPressed;
  pages: Array<{title: string, component: any,icon?:string}>;

  constructor(public platform: Platform) {
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Home', component: Dashboard,icon:'apps' },
      { title: 'Generate Quote', component: Policies ,icon:'list-box' },
      { title: 'My Policies', component: Policies ,icon:'briefcase' },
      { title: 'Availabe Policies', component: Policies ,icon:'logo-buffer' },
      { title: 'Calculators', component: Policies ,icon:'calculator' },
      { title: '', component: undefined ,icon:'' },
      { title: 'My Profile', component: Policies ,icon:'person' },
      { title: 'FAQ', component: FaqPage ,icon:'faq' },
      { title: 'Social', component: Policies ,icon:'share' }
    ];

  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      StatusBar.styleDefault();
      Splashscreen.hide();
      // ------------ Double back button press to exit---------------
      this.platform.registerBackButtonAction(() => {
        if (this.nav.canGoBack()) {
          this.nav.pop()
          return;
        }
        if(!this.backPressed) {
          this.backPressed = true
          //window.plugins.toast.show('Presiona el boton atras de nuevo para cerrar', 'short', 'bottom')
          setTimeout(() => this.backPressed = false, 2000)
          return;
        }
         this.platform.exitApp();
      })      
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component,{}, {animate: true, direction: 'forward'});
  }
}
